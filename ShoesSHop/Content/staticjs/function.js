//var window width
var viewportGlobal = $(window).width();

//function js cal match height
var calMatchHeight = function(){
  if($('.js-match-height').length > 0){
    $('.js-match-height >ul >li').matchHeight();
		 $('.c-address-list .c-address-item').matchHeight();
  }
};

//function js alias mobile expand
var aliasMobileExpand = function(){
  if($('.c-alias').length > 0){
    $('.js-menu-mobile').click(function(e) {
      e.preventDefault();
      var body = $('body');
      var menu = $('.c-alias__expand');
      var page = $('body');
      var ovelay = $('.js-app-ovelay');
      if (page.hasClass('has-page-open')) {
        page.removeClass('has-page-open');
        ovelay.removeClass('has-ovelay-show');
        menu.removeClass('has-menu-open');
        body.removeClass('has-body-open');
        $(this).removeClass('is-active');
      } else {
        page.addClass('has-page-open');
        ovelay.addClass('has-ovelay-show');
        menu.addClass('has-menu-open');
        body.addClass('has-body-open');
        $(this).addClass('is-active');
      }
    });
    $('.js-app-ovelay, .js-alias-close').click(function() {
      var body = $('body');
      var menu = $('.c-alias__expand');
      var page = $('body');
      var ovelay = $('.js-app-ovelay');
      page.removeClass("has-page-open");
      ovelay.removeClass("has-ovelay-show");
      menu.removeClass("has-menu-open");
      body.removeClass("has-body-open");
      $('.js-menu-mobile').removeClass('is-active');
      return false;
    });
  }
};

//function js main slider
var mainSlider = function(){
  if($('.c-slider').length > 0){
    $('#slider-id').owlCarousel({
      items:1,
      loop:true,
      margin:0,
      responsiveClass:false,
      nav:true,
      dots:true,
      autoplay:true,
      autoHeight:false,
      autoplayTimeout:8000,
      autoplaySpeed:1000,
      autoplayHoverPause:false,
      navText:false,
    });
  }
};

//function js cat slider
var catSlider = function(){
  if($('.c-cat-slider').length > 0){
    $('#cat-slider-id').owlCarousel({
      loop:false,
      margin:8,
      responsiveClass:true,
      nav:false,
      dots:true,
      autoplay:false,
      autoHeight:false,
      autoplayTimeout:8000,
      autoplayHoverPause:false,
      navText:false,
      responsive:{
        0:{
          items:2
        },
        768:{
          items:4
        },
        992:{
          items:6
        },
        1200:{
          items:8
        }
      }
    });
  }
};

//function js footer content expand
var footerContentExpand = function(){
  if($('.c-footer-label').length > 0){
    $('.c-footer-label .fas').click(function() {
      var grand = $(this).parent().parent();
      if ($(this).hasClass('fa-plus')) {
        $(this).removeClass('fa-plus');
        $(this).addClass('fa-minus');
        $('.c-footer-content',$(grand)).slideDown();
      } else {
        $(this).removeClass('fa-minus');
        $(this).addClass('fa-plus');
        $('.c-footer-content',$(grand)).slideUp();
      }
    });
  }
};

//function js template slider
var templateSlider = function(){
  if($('.c-template-slider').length > 0){
    $('.js-template-slider').owlCarousel({
      loop:true,
      margin:16,
      responsiveClass:true,
      nav:true,
      dots:false,
      autoplay:true,
      autoHeight:false,
      autoplayTimeout:6000,
      autoplayHoverPause:false,
      navText:false,
      responsive:{
        0:{
          items:2
        },
        768:{
          items:3
        },
        992:{
          items:4
        }
      }
    });
  }
};

//function js count slider
var countSlider = function(){
  if($('.c-template-slider').length > 0){
    $('.js-count-slider').owlCarousel({
      loop:true,
      margin:16,
      responsiveClass:true,
      nav:true,
      dots:false,
      autoplay:true,
      autoHeight:false,
      autoplayTimeout:6000,
      autoplayHoverPause:false,
      navText:false,
      responsive:{
        0:{
          items:1
        },
        768:{
          items:3
        },
        992:{
          items:4
        }
      }
    });
  }
};

//function go top
var goTop = function(){
  $(window).scroll(function () {
    if ($(this).scrollTop() > 150) {
      $('.c-gotop').fadeIn();
    } else {
      $('.c-gotop').fadeOut();
    }
  });
  $('.c-gotop').click(function(e){
    e.preventDefault();
    $("html, body").animate({scrollTop: $('body').offset().top}, 700);
  });
};

//function js sidebar mobile expand
var sidebarMobileExpand = function(){
  if($('.c-filter-mobile').length > 0){
    $('.js-sidebar-mobile a').click(function(e) {
      e.preventDefault();
      var body = $('body');
      var id = $(this).attr('href');
      var menu = $(id);
      var page = $('body');
      var ovelay = $('.js-sidebar-ovelay');
      if (page.hasClass('has-pagefilter-open')) {
        page.removeClass('has-pagefilter-open');
        ovelay.removeClass('has-ovelay-show');
        menu.removeClass('has-menufilter-open');
        body.removeClass('has-body-open');
        $(this).removeClass('is-active');
      } else {
        page.addClass('has-pagefilter-open');
        ovelay.addClass('is-active');
        menu.addClass('has-menufilter-open');
        body.addClass('has-body-open');
        $(this).addClass('is-active');
      }
    });
    $('.js-sidebar-ovelay, .js-sidebar-close').click(function() {
      var body = $('body');
      var menu = $('.c-sidebar-filter');
      var page = $('body');
      var ovelay = $('.js-sidebar-ovelay');
      page.removeClass("has-pagefilter-open");
      ovelay.removeClass("is-active");
      menu.removeClass("has-menufilter-open");
      body.removeClass("has-body-open");
      return false;
    });
  }
};

//smartfilter click
var expandFilter = function(){
  $('.bs-filter > ul > li > .bs-label').click(function() {
    var parent = $(this).parent();
    var grand = $(this).parent().parent().parent();
    if ($(parent).hasClass("active")) {
      $(parent).removeClass("active");
      $('.bs-submenu',$(parent)).hide();
      $('.fa',$(this)).removeClass("fa-caret-up").addClass("fa-caret-down");
    } else {
      $('.bs-submenu',$('.bs-filter > ul > li.active')).hide();
      $('.fa',$('.bs-filter > ul > li.active > .bs-label')).removeClass("fa-caret-up").addClass("fa-caret-down");
      $('> ul > li',$(grand)).removeClass("active");
      $(parent).addClass("active");
      $('.bs-submenu',$(parent)).show();
      $('.fa',$(this)).removeClass("fa-caret-down").addClass("fa-caret-up");
    }
    return false;
  });
  $('body').click(function(e) {
    var formCheck = $('.bs-submenu');
    if (formCheck.is(':visible') && $(e.target).parents('.bs-submenu').length == 0){
      $('.fa',$('.bs-filter > ul > li.active > .bs-label')).removeClass("fa-caret-up").addClass("fa-caret-down");
      $('.bs-filter > ul > li').removeClass("active");
      formCheck.hide();
    }
  });
//expand smartfilter click with ipad, mobile
  $('.js-main-mobile').click(function() {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $('.bs-filter').slideUp();
    } else {
      $(this).addClass("active");
      $('.bs-filter').slideDown();
    }
    return false;
  });
  //smartfilter scroll item
  $('.bs-scroll').slimScroll({
    height: '155px',
    alwaysVisible: true
  });
};

//function load thumb slider
var loadThumbSlider = function(){
  //detail left thumb slider
  if($('.c-product-thumb').length > 0){
    $('#product-thumb-id').owlCarousel({
      loop:false,
      margin:0,
      responsiveClass:true,
      nav:true,
      dots:false,
      autoplay:false,
      autoHeight:false,
      autoplayTimeout:4000,
      autoplayHoverPause:false,
      navText:false,
      responsive:{
        0:{
          items:3
        },
        1200:{
          items:5
        }
      },
    });
  }
};

//function js product thumb click
var productThumbClick = function(){
  if($('.c-product-thumb').length > 0){
    $('.c-thumb-item a').click(function(){
      var parent = $(this).parent();
      var grand = $(this).parent().parent().parent();
      var x = $(this).attr('href');
      $('.c-thumb-item',$(grand)).removeClass('is-active');
      $(parent).addClass("is-active");
      $('.c-product-image img').attr('src',x);
      return false;
    });
  }
};

//function js detail tab scroll
var detailTabScroll = function(){
  if($('.c-product-main').length > 0){
    $('.c-product-main .c-tabs .c-tabs__title ul li a, .c-product-center__desc-btn').on('click',function(e){
      e.preventDefault();
      var id = $(this).attr('href');
      var tabHeight = $('.c-product-main .c-tabs .c-tabs__title').height();
      $("html, body").animate({scrollTop: $(id).offset().top - tabHeight}, 700);
    });
  }
};

//var function Payment Expand
var paymentExpand = function(){
  if($('.c-payments').length > 0){
    $('.js-payment-radio').click(function() {
      $(this).prop('checked', true);
      var parent = $(this).parent().parent();
      if (!parent.hasClass('active')) {
        $('.c-payments .c-payment.active .c-payment__content').slideUp();
        $('.c-payments .c-payment.active').removeClass('active');
        $(parent).addClass('active');
        $('.c-payment__content', parent).slideDown();
      }
    });
    $('.js-address-same').change(function() {
      var parent = $(this).parent().parent();
      if(this.checked) {
        console.log('checked');
        $('.c-address-other', parent).slideUp();
        $('.c-address-details', parent).slideDown();
      }else {
        console.log('not checked');
        $('.c-address-other', parent).slideDown();
        $('.c-address-details', parent).slideUp();
      }
    });
  }
};