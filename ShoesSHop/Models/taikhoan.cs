namespace ShoesSHop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("taikhoan")]
    public partial class taikhoan
    {
        public int id { get; set; }

        [Required]
        [StringLength(255)]
        public string tendangnhap { get; set; }

        [Required]
        [StringLength(255)]
        public string matkhau { get; set; }

        [Required]
        [StringLength(255)]
        public string quyentruycap { get; set; }
    }
}
