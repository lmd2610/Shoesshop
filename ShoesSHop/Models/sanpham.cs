namespace ShoesSHop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("sanpham")]
    public partial class sanpham
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sanpham()
        {
            chitietdonhangs = new HashSet<chitietdonhang>();
            hinhanhsanphams = new HashSet<hinhanhsanpham>();
            khachhangsanphams = new HashSet<khachhangsanpham>();
            blocks = new HashSet<block>();
        }

        public int id { get; set; }

        [StringLength(255)]
        public string tensanpham { get; set; }

        public int? idkichco { get; set; }

        public int? idmausac { get; set; }

        public double? giaban { get; set; }

        [StringLength(255)]
        public string gioithieusanpham { get; set; }

        public int? idnhasanxuat { get; set; }

        public int? idloaisanpham { get; set; }

        public int? idkhuyenmai { get; set; }

        [StringLength(255)]
        public string hinhanh { get; set; }

        public double? giansautruocdo { get; set; }

        public double? phantram { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<chitietdonhang> chitietdonhangs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<hinhanhsanpham> hinhanhsanphams { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<khachhangsanpham> khachhangsanphams { get; set; }

        public virtual khuyenmai khuyenmai { get; set; }

        public virtual kichco kichco { get; set; }

        public virtual loaisanpham loaisanpham { get; set; }

        public virtual mausac mausac { get; set; }

        public virtual nhasanxuat nhasanxuat { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<block> blocks { get; set; }
    }
}
