namespace ShoesSHop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("donhang")]
    public partial class donhang
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public donhang()
        {
            chitietdonhangs = new HashSet<chitietdonhang>();
        }

        public int id { get; set; }

        [StringLength(255)]
        public string tenkhachhang { get; set; }

        [StringLength(20)]
        public string sdt { get; set; }

        [StringLength(255)]
        public string diachi { get; set; }

        public double? tonggia { get; set; }

        public double? phiship { get; set; }

        public double? thanhtoan { get; set; }

        public double? trangthaidonhang { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<chitietdonhang> chitietdonhangs { get; set; }
    }
}
