namespace ShoesSHop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("hinhanhsanpham")]
    public partial class hinhanhsanpham
    {
        public int id { get; set; }

        [Column(TypeName = "text")]
        public string url { get; set; }

        public int? idsanpham { get; set; }

        public virtual sanpham sanpham { get; set; }
    }
}
