namespace ShoesSHop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("khachhangsanpham")]
    public partial class khachhangsanpham
    {
        public int id { get; set; }

        public int? idsanpham { get; set; }

        public int? idkhachhang { get; set; }

        public int? quantity { get; set; }

        public virtual khachhang khachhang { get; set; }

        public virtual sanpham sanpham { get; set; }
    }
}
