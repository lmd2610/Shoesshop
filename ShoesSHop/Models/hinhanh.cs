namespace ShoesSHop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("hinhanh")]
    public partial class hinhanh
    {
        public int id { get; set; }

        [Column(TypeName = "text")]
        public string url { get; set; }
    }
}
