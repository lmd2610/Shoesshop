﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoesSHop.Models
{
    public class Size_SP_Chitietdonhang
    {
        public int iddonhang { get; set; }

        public string kichco1 { get; set; }

        public int idsanpham { get; set; }

        public double? soluong { get; set; }

        public string tensanpham { get; set; }

        public int? idkichco { get; set; }

        public double? giaban { get; set; }
        public string hinhanh { get; set; }
    }
}