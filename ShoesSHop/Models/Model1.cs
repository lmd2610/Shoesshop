namespace ShoesSHop.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Collections.Generic;
    using Microsoft.Ajax.Utilities;
    using System.Text.RegularExpressions;
    using System.Data.SqlClient;
    using System.Net;
    using System.Net.Mail;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model11")
        {
        }

        public virtual DbSet<block> blocks { get; set; }
        public virtual DbSet<chitietdonhang> chitietdonhangs { get; set; }
        public virtual DbSet<danhmuc> danhmucs { get; set; }
        public virtual DbSet<donhang> donhangs { get; set; }
        public virtual DbSet<hinhanh> hinhanhs { get; set; }
        public virtual DbSet<hinhanhsanpham> hinhanhsanphams { get; set; }
        public virtual DbSet<khachhang> khachhangs { get; set; }
        public virtual DbSet<khachhangsanpham> khachhangsanphams { get; set; }
        public virtual DbSet<khuyenmai> khuyenmais { get; set; }
        public virtual DbSet<kichco> kichcoes { get; set; }
        public virtual DbSet<loaisanpham> loaisanphams { get; set; }
        public virtual DbSet<mausac> mausacs { get; set; }
        public virtual DbSet<nhasanxuat> nhasanxuats { get; set; }
        public virtual DbSet<sanpham> sanphams { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<taikhoan> taikhoans { get; set; }
        public virtual DbSet<tintuc> tintucs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<block>()
                .HasMany(e => e.sanphams)
                .WithMany(e => e.blocks)
                .Map(m => m.ToTable("blockdetail").MapLeftKey("idblock").MapRightKey("idsanpham"));

            modelBuilder.Entity<danhmuc>()
                .HasMany(e => e.tintucs)
                .WithOptional(e => e.danhmuc)
                .HasForeignKey(e => e.iddanhmuc);

            modelBuilder.Entity<donhang>()
                .Property(e => e.sdt)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<donhang>()
                .HasMany(e => e.chitietdonhangs)
                .WithRequired(e => e.donhang)
                .HasForeignKey(e => e.iddonhang)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<hinhanh>()
                .Property(e => e.url)
                .IsUnicode(false);

            modelBuilder.Entity<hinhanhsanpham>()
                .Property(e => e.url)
                .IsUnicode(false);

            modelBuilder.Entity<khachhang>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<khachhang>()
                .Property(e => e.password)
                .IsUnicode(false);

            modelBuilder.Entity<khachhang>()
                .HasMany(e => e.khachhangsanphams)
                .WithOptional(e => e.khachhang)
                .HasForeignKey(e => e.idkhachhang);

            modelBuilder.Entity<khuyenmai>()
                .HasMany(e => e.sanphams)
                .WithOptional(e => e.khuyenmai)
                .HasForeignKey(e => e.idkhuyenmai);

            modelBuilder.Entity<kichco>()
                .HasMany(e => e.sanphams)
                .WithOptional(e => e.kichco)
                .HasForeignKey(e => e.idkichco);

            modelBuilder.Entity<loaisanpham>()
                .HasMany(e => e.sanphams)
                .WithOptional(e => e.loaisanpham)
                .HasForeignKey(e => e.idloaisanpham);

            modelBuilder.Entity<mausac>()
                .HasMany(e => e.sanphams)
                .WithOptional(e => e.mausac)
                .HasForeignKey(e => e.idmausac);

            modelBuilder.Entity<nhasanxuat>()
                .HasMany(e => e.loaisanphams)
                .WithOptional(e => e.nhasanxuat)
                .HasForeignKey(e => e.idnhasanxuat);

            modelBuilder.Entity<nhasanxuat>()
                .HasMany(e => e.sanphams)
                .WithOptional(e => e.nhasanxuat)
                .HasForeignKey(e => e.idnhasanxuat);

            modelBuilder.Entity<sanpham>()
                .Property(e => e.hinhanh)
                .IsFixedLength();

            modelBuilder.Entity<sanpham>()
                .HasMany(e => e.chitietdonhangs)
                .WithRequired(e => e.sanpham)
                .HasForeignKey(e => e.idsanpham)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<sanpham>()
                .HasMany(e => e.hinhanhsanphams)
                .WithOptional(e => e.sanpham)
                .HasForeignKey(e => e.idsanpham);

            modelBuilder.Entity<sanpham>()
                .HasMany(e => e.khachhangsanphams)
                .WithOptional(e => e.sanpham)
                .HasForeignKey(e => e.idsanpham);

            modelBuilder.Entity<taikhoan>()
                .Property(e => e.tendangnhap)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<taikhoan>()
                .Property(e => e.matkhau)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<taikhoan>()
                .Property(e => e.quyentruycap)
                .IsFixedLength()
                .IsUnicode(false);
        }
        public class Fhinhanh
        {
            Model1 context = new Model1();
            public IEnumerable<hinhanh> getHinhanh()
            {
                return context.hinhanhs;
            }
        }
        public class Fdanhmuc
        {
            Model1 context = new Model1();
            public IEnumerable<danhmuc> getDanhmuc()
            {
                return context.danhmucs;
            }
        }
        public class Fsanpham
        {
            Model1 context = new Model1();
            public IEnumerable<sanpham> getSanpham()
            {
                return context.sanphams;
            }
        }
        public class FKhachhang
        {
            Model1 context = new Model1();
            public IEnumerable<khachhang> login()
            {
                return context.khachhangs;
            }
            
            public khachhang TimKhachHang(string id, string pass)
            {
                return context.khachhangs.Where(a => a.email.Equals(id) && a.password.Equals(pass)).FirstOrDefault();
            }
            public bool KiemTraEmail(string email)
            {
                return Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
            }
            public void LayLaiMatKhau(string email)
            {
                SqlParameter[] idParam = {
                new SqlParameter {ParameterName = "Email",Value = email}};
                context.Database.ExecuteSqlCommand("LayLaiMatKhau @Email", idParam);
            }

            public void ThemKhachHang(khachhang khach)
            {
               
                context.khachhangs.Add(khach);
                context.SaveChanges();
            }
            public khachhang GetKH(int id)
            {
                return context.khachhangs.Find(id);
            }

            public bool ThayDoiMatKhau(string email)
            {
                khachhang khach = context.khachhangs.Where(x => x.email == email).FirstOrDefault();
                if (khach != null)
                {
                    khach.password = "123sada";
                    context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            public void SendEmail(string email)
            {
                MailMessage mailMessage = new MailMessage("laminhduc2610@gmail.com", email);
                mailMessage.Subject = "Test";
                mailMessage.Body = "This is password : 123sada";
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Send(mailMessage);
            }
        }
    }
}
