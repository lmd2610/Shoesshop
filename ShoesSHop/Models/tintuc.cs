namespace ShoesSHop.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tintuc")]
    public partial class tintuc
    {
        public int id { get; set; }

        [StringLength(255)]
        public string tieude { get; set; }

        [StringLength(255)]
        public string noidung { get; set; }

        public int? iddanhmuc { get; set; }

        public virtual danhmuc danhmuc { get; set; }
    }
}
