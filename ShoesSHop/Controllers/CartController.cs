﻿using ShoesSHop.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebGrease;

namespace ShoesSHop.Controllers
{
    public class CartController : Controller
    {
        // GET: Cart
        private string CartSession = "CartSession";
        Model1 db = new Model1();
        public List<Size_SP_Chitietdonhang> GetList()
        {
            var query = from sp in db.sanphams
                        join size in db.kichcoes on sp.idkichco equals size.id
                        join ctdh in db.chitietdonhangs on sp.id equals ctdh.idsanpham
                        select new Size_SP_Chitietdonhang
                        {
                            tensanpham = sp.tensanpham,
                            giaban = sp.giaban,
                            hinhanh = sp.hinhanh,
                            kichco1 = size.kichco1,
                            soluong = ctdh.soluong
                    
                        };
            return query.ToList();
                        
        }
        public double? TinhTong(List<Size_SP_Chitietdonhang> list)
        {
            return list.Sum(x => x.giaban * x.soluong);
        }

        public ActionResult Cart1()
        {
            var cart = Session[CartSession];
            var list = new List<Size_SP_Chitietdonhang>();

            list = GetList();
            ViewBag.TongTien = TinhTong(list);
            if (cart != null)
            {
                list = (List<Size_SP_Chitietdonhang>)cart;
                list = GetList();
            }
           
            return View(list);
        }
        public ActionResult Cart2()
        {
            return View();
        }
        public ActionResult Cart3()
        {
            return View();
        }
        public ActionResult CartAddress()
        {
            return View();
        }
        public ActionResult Message()
        {
            return View();
        }
        public ActionResult AddItem(int productId, int quantity)
        {
            var cart = Session[CartSession];
            if (cart != null)
            {
                var list = (List<chitietdonhang>)cart;
                if(list.Exists(x=>x.idsanpham == productId))
                {
                    foreach (var item in list)
                    {
                        if (item.idsanpham == productId)
                        {
                            item.soluong += quantity;
                        }
                    }
                }
                else
                {
                    // tao moi doi tuong cart item
                    var item = new chitietdonhang();
                    item.idsanpham = productId;
                    item.soluong = quantity;
                    list.Add(item);
                }
                
            }
            else
            {
                var item = new chitietdonhang();
                item.idsanpham = productId;
                item.soluong = quantity;
                var list = new List<chitietdonhang>();
                Session[CartSession] = list;
            }
            return RedirectToAction("Cart2");
        }
    }
}