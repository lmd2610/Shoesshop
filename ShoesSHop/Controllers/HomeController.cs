﻿using ShoesSHop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static ShoesSHop.Models.Model1;

namespace ShoesSHop.Controllers
{
    public class HomeController : Controller
    {
        private Model1 context = new Model1();
        public ActionResult Index()
        {
            Fdanhmuc fdanhmuc = new Fdanhmuc();
            Fhinhanh fhinhanh = new Fhinhanh();
            Fsanpham fsanpham = new Fsanpham();
       
            ViewBag.danhmuc = fdanhmuc.getDanhmuc();
            ViewBag.hinhanh = fhinhanh.getHinhanh();
            ViewBag.sanpham = fsanpham.getSanpham();
            return View();
        }
        
        
    }
}