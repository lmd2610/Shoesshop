﻿using ShoesSHop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using static ShoesSHop.Models.Model1;

namespace ShoesSHop.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            ViewBag.Breadcumb = "Đăng nhập";
            return View();
        }
        public ActionResult Register()
        {
            ViewBag.Breadcumb = "Đăng kí";
            return View();
        }
        public ActionResult FgPassword()
        {
            ViewBag.Breadcumb = "Quên mật khẩu";
            return View();
        }
        [HttpPost]
        public ActionResult FgPassword1(string email)
        {
            FKhachhang fKhachHang = new FKhachhang();
            if (fKhachHang.ThayDoiMatKhau(email))
            {
                fKhachHang.SendEmail(email);
                
            }
            return RedirectToAction("Index", new RouteValueDictionary(
                                new { controller = "Home", action = "Index" }));

        }


       
        public ActionResult Login(string email, string password, bool checkbox=false)
        {

            FKhachhang fKhachHang = new FKhachhang();
            khachhang x = fKhachHang.TimKhachHang(email, password);
            if (x != null)
            {
                if (checkbox)
                {
                    Response.Cookies["ID"].Value = x.email.ToString();
                    Response.Cookies["ID"].Expires = DateTime.Now.AddDays(1);
                    return RedirectToAction("Home");
                }
                else
                {
                    Session["KhachHang"] = x;
                    //return RedirectToAction("Home");
                    return RedirectToAction("Index", new RouteValueDictionary(
                            new { controller = "Home", action = "Index"}));
                }
            }
            else
            {
                //return RedirectToAction("Login");
                return RedirectToAction("", new RouteValueDictionary(
                            new { controller = "Login", action = "Login" }));
            }
        }
        [HttpPost]
        public ActionResult Signup1(string email, string name, string diachi, string sdt, string password,string password2)
        {
            FKhachhang fKhachHang = new FKhachhang();
            
            khachhang khach= new khachhang() { email = email, ten = name, sdt = sdt, diachi = diachi, password = password };
            fKhachHang.ThemKhachHang(khach);
            return RedirectToAction("", new RouteValueDictionary(
                            new { controller = "Login", action = "Login" }));
        }
        public ActionResult Signup()
        {
            return View();
        }
    }
}