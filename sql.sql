
Create database shopgiay
go
USE [shopgiay]
GO
/****** Object:  Table [dbo].[block]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[block](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tenblock] [nvarchar](255) NULL,
	[indexs] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[blockdetail]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[blockdetail](
	[idblock] [int] NOT NULL,
	[idsanpham] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idblock] ASC,
	[idsanpham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[chitietdonhang]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chitietdonhang](
	[iddonhang] [int] NOT NULL,
	[idsanpham] [int] NOT NULL,
	[soluong] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[iddonhang] ASC,
	[idsanpham] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[danhmuc]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[danhmuc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tendanhmuc] [nvarchar](255) NULL,
	[url] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[donhang]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[donhang](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tenkhachhang] [nvarchar](255) NULL,
	[sdt] [char](20) NULL,
	[diachi] [nvarchar](255) NULL,
	[tonggia] [float] NULL,
	[phiship] [float] NULL,
	[thanhtoan] [float] NULL,
	[trangthaidonhang] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[hinhanh]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[hinhanh](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[url] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[hinhanhsanpham]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[hinhanhsanpham](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[url] [text] NULL,
	[idsanpham] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[khachhang]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[khachhang](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ten] [nvarchar](255) NULL,
	[email] [varchar](255) NULL,
	[password] [varchar](255) NULL,
	[diachi] [nvarchar](255) NULL,
	[sdt] [nchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[khachhangsanpham]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[khachhangsanpham](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idsanpham] [int] NULL,
	[idkhachhang] [int] NULL,
	[quantity] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[khuyenmai]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[khuyenmai](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tenkhuyenmai] [nvarchar](255) NULL,
	[phantram] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[kichco]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[kichco](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[kichco] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[loaisanpham]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[loaisanpham](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tenloai] [nvarchar](255) NULL,
	[idnhasanxuat] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mausac]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mausac](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[mausac] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[nhasanxuat]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nhasanxuat](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tennhasanxuat] [nvarchar](255) NULL,
	[gioithieu] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sanpham]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sanpham](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tensanpham] [nvarchar](255) NULL,
	[idkichco] [int] NULL,
	[idmausac] [int] NULL,
	[giaban] [float] NULL,
	[gioithieusanpham] [nvarchar](255) NULL,
	[idnhasanxuat] [int] NULL,
	[idloaisanpham] [int] NULL,
	[idkhuyenmai] [int] NULL,
	[hinhanh] [nchar](255) NULL,
	[giansautruocdo] [float] NULL,
	[phantram] [float] NULL,
 CONSTRAINT [PK__sanpham__3213E83F9D36317D] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[taikhoan]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[taikhoan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tendangnhap] [char](255) NOT NULL,
	[matkhau] [char](255) NOT NULL,
	[quyentruycap] [char](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tintuc]    Script Date: 10/06/2020 11:42:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tintuc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tieude] [nvarchar](255) NULL,
	[noidung] [nvarchar](255) NULL,
	[iddanhmuc] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[danhmuc] ON 

INSERT [dbo].[danhmuc] ([id], [tendanhmuc], [url]) VALUES (1, N'Giày da nam', N'~/Content/images/danhmuc/2.jpg')
INSERT [dbo].[danhmuc] ([id], [tendanhmuc], [url]) VALUES (2, N'Giày da nữ', N'~/Content/images/danhmuc/4.jpg')
INSERT [dbo].[danhmuc] ([id], [tendanhmuc], [url]) VALUES (3, N'Giày thể thao nam', N'~/Content/images/danhmuc/1.jpg')
INSERT [dbo].[danhmuc] ([id], [tendanhmuc], [url]) VALUES (4, N'Giày thể thao nữ', N'~/Content/images/danhmuc/3.jpg')
SET IDENTITY_INSERT [dbo].[danhmuc] OFF
SET IDENTITY_INSERT [dbo].[donhang] ON 

INSERT [dbo].[donhang] ([id], [tenkhachhang], [sdt], [diachi], [tonggia], [phiship], [thanhtoan], [trangthaidonhang]) VALUES (1, N'Lã Minh Đức', N'0356798938          ', N'DDDDd', 2000000, 10000, 200000010, 1)
SET IDENTITY_INSERT [dbo].[donhang] OFF
SET IDENTITY_INSERT [dbo].[hinhanh] ON 

INSERT [dbo].[hinhanh] ([id], [url]) VALUES (1, N'~/Content/upload/slider_1.jpg')
INSERT [dbo].[hinhanh] ([id], [url]) VALUES (2, N'~/Content/upload/slider_1.jpg')
INSERT [dbo].[hinhanh] ([id], [url]) VALUES (3, N'~/Content/upload/slider_1.jpg')
INSERT [dbo].[hinhanh] ([id], [url]) VALUES (4, N'~/Content/upload/slider_1.jpg')
SET IDENTITY_INSERT [dbo].[hinhanh] OFF
SET IDENTITY_INSERT [dbo].[hinhanhsanpham] ON 

INSERT [dbo].[hinhanhsanpham] ([id], [url], [idsanpham]) VALUES (1, N'~/Content/upload/product_1.jpg', 1)
INSERT [dbo].[hinhanhsanpham] ([id], [url], [idsanpham]) VALUES (2, N'~/Content/upload/product_1.jpg', 2)
SET IDENTITY_INSERT [dbo].[hinhanhsanpham] OFF
SET IDENTITY_INSERT [dbo].[khachhang] ON 

INSERT [dbo].[khachhang] ([id], [ten], [email], [password], [diachi], [sdt]) VALUES (1, N'duc', N'laminhduc2610@gmail.com', N'123sada', N'Số nhà 230 tổ 11 phường Phú Xá Thành phố Thái Nguyên Tỉnh Thái Nguyên', NULL)
INSERT [dbo].[khachhang] ([id], [ten], [email], [password], [diachi], [sdt]) VALUES (2, N'Lã Minh Đức', N'lmd2610@gmail.com', N'laduc123', N'Sốewer', N'0356798938')
SET IDENTITY_INSERT [dbo].[khachhang] OFF
SET IDENTITY_INSERT [dbo].[khuyenmai] ON 

INSERT [dbo].[khuyenmai] ([id], [tenkhuyenmai], [phantram]) VALUES (1, N'Sale off 50%', 50)
INSERT [dbo].[khuyenmai] ([id], [tenkhuyenmai], [phantram]) VALUES (2, N'Flash sale', 30)
SET IDENTITY_INSERT [dbo].[khuyenmai] OFF
SET IDENTITY_INSERT [dbo].[kichco] ON 

INSERT [dbo].[kichco] ([id], [kichco]) VALUES (1, N'X')
INSERT [dbo].[kichco] ([id], [kichco]) VALUES (2, N'S')
INSERT [dbo].[kichco] ([id], [kichco]) VALUES (3, N'XS')
INSERT [dbo].[kichco] ([id], [kichco]) VALUES (4, N'XL')
INSERT [dbo].[kichco] ([id], [kichco]) VALUES (5, N'XLL')
SET IDENTITY_INSERT [dbo].[kichco] OFF
SET IDENTITY_INSERT [dbo].[loaisanpham] ON 

INSERT [dbo].[loaisanpham] ([id], [tenloai], [idnhasanxuat]) VALUES (1, N'Giày da cho nam', 1)
INSERT [dbo].[loaisanpham] ([id], [tenloai], [idnhasanxuat]) VALUES (2, N'Giày da cho nữ', 1)
INSERT [dbo].[loaisanpham] ([id], [tenloai], [idnhasanxuat]) VALUES (3, N'Giày thể thao cho nam', 2)
INSERT [dbo].[loaisanpham] ([id], [tenloai], [idnhasanxuat]) VALUES (5, N'Giày thể thao cho nữ', 2)
SET IDENTITY_INSERT [dbo].[loaisanpham] OFF
SET IDENTITY_INSERT [dbo].[mausac] ON 

INSERT [dbo].[mausac] ([id], [mausac]) VALUES (1, N'Đỏ')
INSERT [dbo].[mausac] ([id], [mausac]) VALUES (2, N'Cam')
INSERT [dbo].[mausac] ([id], [mausac]) VALUES (3, N'Lam')
INSERT [dbo].[mausac] ([id], [mausac]) VALUES (4, N'Trắng')
INSERT [dbo].[mausac] ([id], [mausac]) VALUES (5, N'Neon')
INSERT [dbo].[mausac] ([id], [mausac]) VALUES (6, N'Bảy màu')
SET IDENTITY_INSERT [dbo].[mausac] OFF
SET IDENTITY_INSERT [dbo].[nhasanxuat] ON 

INSERT [dbo].[nhasanxuat] ([id], [tennhasanxuat], [gioithieu]) VALUES (1, N'Pazzini', N'1')
INSERT [dbo].[nhasanxuat] ([id], [tennhasanxuat], [gioithieu]) VALUES (2, N'Channel', N'2')
INSERT [dbo].[nhasanxuat] ([id], [tennhasanxuat], [gioithieu]) VALUES (3, N'Gucci', N'2')
SET IDENTITY_INSERT [dbo].[nhasanxuat] OFF
SET IDENTITY_INSERT [dbo].[sanpham] ON 

INSERT [dbo].[sanpham] ([id], [tensanpham], [idkichco], [idmausac], [giaban], [gioithieusanpham], [idnhasanxuat], [idloaisanpham], [idkhuyenmai], [hinhanh], [giansautruocdo], [phantram]) VALUES (1, N'giày nam gucci', 1, 1, 2000000, N'Là sản phẩm bán với số lượng có hạn', 1, 1, 1, N'~/Content/upload/product_1.jpg                                                                                                                                                                                                                                 ', 22000000, 20)
INSERT [dbo].[sanpham] ([id], [tensanpham], [idkichco], [idmausac], [giaban], [gioithieusanpham], [idnhasanxuat], [idloaisanpham], [idkhuyenmai], [hinhanh], [giansautruocdo], [phantram]) VALUES (2, N'giày nữ gucci', 1, 1, 20000000, N'Là sản phẩm phiên bản limited được thiết kế bởi nghệ sĩ nổi tiếng', 1, 1, 1, N'                                                                                                                                                                                                                                                               ', 20000000, 20)
SET IDENTITY_INSERT [dbo].[sanpham] OFF
SET IDENTITY_INSERT [dbo].[taikhoan] ON 

INSERT [dbo].[taikhoan] ([id], [tendangnhap], [matkhau], [quyentruycap]) VALUES (1, N'root                                                                                                                                                                                                                                                           ', N'laduc123                                                                                                                                                                                                                                                       ', N'1                                                                                                                                                                                                                                                              ')
SET IDENTITY_INSERT [dbo].[taikhoan] OFF
ALTER TABLE [dbo].[blockdetail]  WITH CHECK ADD FOREIGN KEY([idblock])
REFERENCES [dbo].[block] ([id])
GO
ALTER TABLE [dbo].[blockdetail]  WITH CHECK ADD  CONSTRAINT [FK__blockdeta__idsan__34C8D9D1] FOREIGN KEY([idsanpham])
REFERENCES [dbo].[sanpham] ([id])
GO
ALTER TABLE [dbo].[blockdetail] CHECK CONSTRAINT [FK__blockdeta__idsan__34C8D9D1]
GO
ALTER TABLE [dbo].[chitietdonhang]  WITH CHECK ADD FOREIGN KEY([iddonhang])
REFERENCES [dbo].[donhang] ([id])
GO
ALTER TABLE [dbo].[chitietdonhang]  WITH CHECK ADD  CONSTRAINT [FK__chitietdo__idsan__38996AB5] FOREIGN KEY([idsanpham])
REFERENCES [dbo].[sanpham] ([id])
GO
ALTER TABLE [dbo].[chitietdonhang] CHECK CONSTRAINT [FK__chitietdo__idsan__38996AB5]
GO
ALTER TABLE [dbo].[hinhanhsanpham]  WITH CHECK ADD  CONSTRAINT [FK__hinhanhsa__idsan__30F848ED] FOREIGN KEY([idsanpham])
REFERENCES [dbo].[sanpham] ([id])
GO
ALTER TABLE [dbo].[hinhanhsanpham] CHECK CONSTRAINT [FK__hinhanhsa__idsan__30F848ED]
GO
ALTER TABLE [dbo].[khachhangsanpham]  WITH CHECK ADD FOREIGN KEY([idkhachhang])
REFERENCES [dbo].[khachhang] ([id])
GO
ALTER TABLE [dbo].[khachhangsanpham]  WITH CHECK ADD FOREIGN KEY([idsanpham])
REFERENCES [dbo].[sanpham] ([id])
GO
ALTER TABLE [dbo].[loaisanpham]  WITH CHECK ADD FOREIGN KEY([idnhasanxuat])
REFERENCES [dbo].[nhasanxuat] ([id])
GO
ALTER TABLE [dbo].[sanpham]  WITH CHECK ADD  CONSTRAINT [FK__sanpham__idkhuye__2E1BDC42] FOREIGN KEY([idkhuyenmai])
REFERENCES [dbo].[khuyenmai] ([id])
GO
ALTER TABLE [dbo].[sanpham] CHECK CONSTRAINT [FK__sanpham__idkhuye__2E1BDC42]
GO
ALTER TABLE [dbo].[sanpham]  WITH CHECK ADD  CONSTRAINT [FK__sanpham__idkichc__2A4B4B5E] FOREIGN KEY([idkichco])
REFERENCES [dbo].[kichco] ([id])
GO
ALTER TABLE [dbo].[sanpham] CHECK CONSTRAINT [FK__sanpham__idkichc__2A4B4B5E]
GO
ALTER TABLE [dbo].[sanpham]  WITH CHECK ADD  CONSTRAINT [FK__sanpham__idloais__2D27B809] FOREIGN KEY([idloaisanpham])
REFERENCES [dbo].[loaisanpham] ([id])
GO
ALTER TABLE [dbo].[sanpham] CHECK CONSTRAINT [FK__sanpham__idloais__2D27B809]
GO
ALTER TABLE [dbo].[sanpham]  WITH CHECK ADD  CONSTRAINT [FK__sanpham__idmausa__2B3F6F97] FOREIGN KEY([idmausac])
REFERENCES [dbo].[mausac] ([id])
GO
ALTER TABLE [dbo].[sanpham] CHECK CONSTRAINT [FK__sanpham__idmausa__2B3F6F97]
GO
ALTER TABLE [dbo].[sanpham]  WITH CHECK ADD  CONSTRAINT [FK__sanpham__idnhasa__2C3393D0] FOREIGN KEY([idnhasanxuat])
REFERENCES [dbo].[nhasanxuat] ([id])
GO
ALTER TABLE [dbo].[sanpham] CHECK CONSTRAINT [FK__sanpham__idnhasa__2C3393D0]
GO
ALTER TABLE [dbo].[tintuc]  WITH CHECK ADD FOREIGN KEY([iddanhmuc])
REFERENCES [dbo].[danhmuc] ([id])
GO
